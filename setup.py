from from keras.models import Model
from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Dense

def build_model():
    input = Input(shape=(32,))
    hidden_layer_1 = Conv2D(?,
                            ?)(input)
    output = Dense(32)(hidden_layer_3)

    model = Model(inputs=input,
                  outputs=output)
    return model