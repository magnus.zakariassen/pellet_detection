from keras.layers import Input
from keras.layers import Conv2D
from keras.layers import Dense
from keras.models import Model


def build_simple_model(input_shape=(27, 27, 1)):
    """
    Builds a simple neural network

    Parameters
    ----------
    input_shape: Array
    Shape of the input on the form of (x, y, z)

    Returns
    -------
    simple_model: <class 'keras.engine.training.Model'>
    Neural network for training


    """
    inputs = Input(shape=input_shape)
    hidden_layer_1 = Conv2D(filters=64,
                            kernel_size=(3, 3),
                            activation='relu')(inputs)
    prediction = Dense(2,
                       activation='softmax')(hidden_layer_1)

    # Builds the model
    simple_model = Model(inputs=inputs,
                         outputs=prediction)
    return simple_model


if __name__ == '__main__':
    model = build_simple_model()
    print(type(model))
    #assert type(model) == 'keras.engine.training.Model'

